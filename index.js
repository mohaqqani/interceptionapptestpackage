let watson = function () {
    console.log("From CND");
    $("#_from").datepicker({maxDate: new Date});
    $("#_to").datepicker({maxDate: new Date});
    var start = $('#_from').val(moment(new Date()).add(-1, 'days').format('MM/DD/YYYY'));
    var end = $('#_to').val(moment(new Date()).format('MM/DD/YYYY'));


    var storeData = [];
    var map, pointarray, heatmap;

    var populateMap = function (data) {

        var interceptionCoordinates = [];

        var storeNames = _.keysIn(_.groupBy(data, 'storeName'));


        _.forEach(data, function (interception, i) {
            interceptionCoordinates.push(new google.maps.LatLng(interception.xCoordinates, interception.yCoordinates));
        });

        var mapOptions = {
            zoom: 11,
            center: interceptionCoordinates[1],
            mapTypeId: google.maps.MapTypeId.Map
        };


        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);


        var pointArray = new google.maps.MVCArray(interceptionCoordinates);

        heatmap = new google.maps.visualization.HeatmapLayer({
            data: pointArray
        });

        for (var i in storeData) {
            var store = storeData[i];
            if (_.includes(storeNames, store.storeName.split(" ").join("_"))) {

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(store.xcoordinate, store.ycoordinate),
                    map: map
                });
            }
        }

        heatmap.setMap(map);
        heatmap.set('radius', 30);

    };


    /*=============Data Calls========================*/
    var getData = function () {
        ajaxFunctionWithoutParam("/watson/store", "JSON", "GET", false, function (data) {
            storeData = data;
        });
        ajaxFunction("/watson/getGeoCoord", {
            city: getCity,
            baID: getBA,
            startDate: getStartDate,
            endDate: getEndDate
        }, "JSON", "GET", false, function (data) {

            if (data.length == 0) {
                alert("No data available");
            } else {
                populateMap(data);
            }
        });

    };

    /*========Filters getter and Setters==============*/

    var getCity = function () {
        return $('#_cityChange').val();
    };

    var getStore = function () {
        return $('#_storeChange').val().split(' ').join('_');
    };

    var getBA = function () {
        return $('#_baChange').val();
    };
    var getStartDate = function () {
        var from = $('#_from').val();
        var fromAr = from.split('/');
        var newFrom = fromAr[2] + '-' + fromAr[0] + '-' + fromAr[1];
        return newFrom;
    };

    var getEndDate = function () {
        var to = $('#_to').val();
        var toAr = to.split('/');
        var newTo = toAr[2] + '-' + toAr[0] + '-' + toAr[1];
        return newTo;
    };

    var setCities = function (data) {
        var names = '';
        $('#_cityChange').empty();

        _.forEach(data, function (item, i) {
            names += "<option value=" + item.city + ">" + item.city + "</option>";
        });
        $('#_cityChange').append(names);
    };

    var setStore = function (data) {
        $('#_storeChange').empty();
        var names = '';
        var storeName = '';
        _.forEach(data, function (item, i) {
            names += "<option value=" + item.storeName.replace(/ /g, "_") + ">" + item.storeName + "</option>";
        });
        $('#_storeChange').append(names);
    };

    var setBA = function (data) {
        $('#_baChange').empty();
        var names = '';
        _.forEach(data, function (item, i) {
            names += "<option value=" + item.id + ">" + item.username + "</option>";
        });
        $('#_baChange').append(names);
    };

    $(document).on("click", '#_dateSearch', function (e) {
        getData();
    });

    $(document).on('click', '#_cityChange', function (e) {
        ajaxFunction("/reporting/getActiveBA", {city: getCity, status:'All'}, "JSON", "GET", false, setBA);
    });

    var getFilters = function () {
        ajaxFunctionWithoutParam("/getCities", "JSON", "GET", false, setCities);
        ajaxFunction("/reporting/getActiveBA", {city: getCity, status:'All'}, "JSON", "GET", false, setBA);
        ajaxFunction("/filters", {city: getCity}, "JSON", "GET", false, setStore);
        getData();
    };
    /*=======PageLoad Calls===========*/
    getFilters();
}

module.exports = watson;